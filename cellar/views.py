from django.shortcuts import render, redirect, get_object_or_404
from cellar.models import Bottle
from cellar.forms import BottleForm
from django.contrib.auth.decorators import login_required


def bottle_list(request):
    bottles = Bottle.objects.all()
    context = {
        "bottle_list": bottles,
    }
    return render(request, "cellar/bottle_list.html", context)


def show_bottle(request, id):
    bottle = get_object_or_404(Bottle, id=id)
    context = {
        "bottle_object": bottle,
    }
    return render(request, "cellar/bottle_detail.html", context)


@login_required
def add_bottle(request):
    if request.method == "POST":
        form = BottleForm(request.POST)
        if form.is_valid():
            bottle = form.save(False)
            bottle.owner = request.user
            bottle.save()
        return redirect("bottle_list")
    else:
        form = BottleForm()

    context = {
        "form": form,
    }

    return render(request, "cellar/add_bottle.html", context)


def edit_tasting(request, id):
    bottle = get_object_or_404(Bottle, id=id)
    if request.method == "POST":
        form = BottleForm(request.POST, instance=bottle)
        if form.is_valid():
            form.save()
        return redirect("recipe_list")
    else:
        form = BottleForm(instance=bottle)

    context = {
            "bottle_object": bottle,
            "bottle_form": form,
        }
    return render(request, "cellar/tasting.html", context)


@login_required
def my_bottle_list(request):
    bottles = Bottle.objects.filter(author=request.user)
    context = {
        "bottle_list": bottles,
    }
    return render(request, "cellar/bottle_list.html", context)
