from django.urls import path
from cellar.views import bottle_list, add_bottle, edit_tasting
from cellar.views import my_bottle_list, show_bottle


urlpatterns = [
    path("", bottle_list, name="bottle_list"),
    path("<int:id>/", show_bottle, name="show_bottle"),
    path("add_bottle/", add_bottle, name="add_bottle"),
    path("<int:id>/edit/", edit_tasting, name="edit_tasting"),
    path("my_cellar/", my_bottle_list, name="my_bottle_list"),
]
