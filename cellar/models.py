from django.db import models
from django.conf import settings


class Bottle(models.Model):
    vintage = models.CharField(max_length=20)
    producer = models.CharField(max_length=50)
    bottle_name = models.CharField(max_length=50)
    picture = models.URLField()
    description = models.TextField()
    cellared_on = models.DateTimeField(auto_now_add=True)
    drank_on = models.DateTimeField()

    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="bottles",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title


# class RecipeStep(models.Model):
    # step_number = models.PositiveSmallIntegerField()
    # instruction = models.TextField()
    # recipe = models.ForeignKey(
    #     Bottle,
    #     related_name="steps",
    #     on_delete=models.CASCADE,
    # )

    # class Meta:
    #     ordering = ["step_number"]


class tasting(models.Model):
    vintage = models.CharField(max_length=10)
    bottle = models.CharField(max_length=20)
    decanted = models.CharField(max_length=10)
    drink_again = models.CharField(max_length=100)
    description = models.TextField()

    bottle = models.ForeignKey(
        Bottle,
        related_name="tasting",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title
