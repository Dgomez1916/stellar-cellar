from django.forms import ModelForm
from cellar.models import Bottle


class BottleForm(ModelForm):
    class Meta:
        model = Bottle
        fields = [
            "vintage",
            "producer"
            "bottle name"
            "picture",
            "description",
            "cellared on"
        ]
